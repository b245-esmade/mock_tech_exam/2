let collection = [];

// Write the queue functions below.

function print() {
    // It will show the array
    console.log(collection) 
    return collection 
}


function enqueue(element) {
    //In this funtion you are going to make an algo that will add an element to the array
    // Mimic the function of push method
    let arrayLength = collection.length;
    collection[arrayLength] = element;
    return collection
}


function dequeue() {
    // In here you are going to remove the last element in the array
    let newArray = []
    let arrayLength = collection.length-1;

    for(let i = 1; i <= arrayLength; i++){
        newArray[i-1] = collection[i]
    }
    collection = newArray
    return collection

}

function front() {
    // In here, you are going to remove the first element

    return collection[0]
}

// starting from here, di na pwede gumamit ng .length property
function size() {
     // Number of elements   
    let arrayLength = 0;
    while(collection[arrayLength]!==undefined){
        arrayLength++;
    }
    return arrayLength;

}

function isEmpty() {
    //it will check whether the function is empty or not

    if(collection.length > 0){
        return false
    }
    return true

}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};